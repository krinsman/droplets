from .norms_and_averages import normalize, relative_error, entrywise_average
from .random import spearman, pearson, covariance, rank_covariance
