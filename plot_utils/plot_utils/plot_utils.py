import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# WARNING: horribly monkey patch follows below, please forgive me if you can
# see explanation: https://stackoverflow.com/a/52818574
def initialize_kde(kde_bounds):
    """The following leads to slightly more accurate/less misleading VIOLIN plots when mass is concentrated near boundaries, as happens often in this notebook. (Doesn't also affect the KDE used in sns.kdeplot because that would be convenient.) I don't really like the programming with side effects though but whatever I guess.
    
    Note that in general this needs to be paired with `cut=0` argument/parameter
    in sns.violinplot in order to not look bad
    """
    # https://github.com/mwaskom/seaborn/issues/525#issuecomment-97651992
    fit_kde_func = sns.categorical._ViolinPlotter.fit_kde

    lb, ub = kde_bounds

    def reflected_once_kde(self, x, bw):
        kde, bw_used = fit_kde_func(self, x, bw)

        kde_evaluate = kde.evaluate

        def truncated_kde_evaluate(x):
            """basically idea seems to be to allocate sum of probability that original Gaussian kernel gave to $-x$ and $+x$ all to $+x$, so that we don't lose probability when cutting off everything beyond 0."""
            val = np.where((x >= lb) & (x <= ub), kde_evaluate(x), 0)
            val += np.where((x >= lb) & (x <= ub), kde_evaluate(lb - x), 0)
            val += np.where((x > lb) & (x <= ub), kde_evaluate(ub - (x - ub)), 0)
            return val

        kde.evaluate = truncated_kde_evaluate
        return kde, bw_used

    sns.categorical._ViolinPlotter.fit_kde = reflected_once_kde