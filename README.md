The included Conda [`environment.yml`](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#exporting-the-environment-yml-file) should allow this to be run using [Binder](https://mybinder.org).

`catpaw_mahjong` = hTPMH
`CNBDM` = hNBDM
`CECNBDM` = hExhNBDM
`CPDM` = hPoDM
`CECPDM` = hExhPoDm
`CPIDM` = hPoID = hierarchical Interaction-Dirichlet-Multinomial

I didn't update the filenames for fear of breaking old references to them.

Progress was "nonlinear", so the filename and directory organization is abysmal. Please e-mail me at krinsman@berkeley.edu if you have any questions.